const Random = require('../lib/random');
const Mock = require("./mock");

describe("Food", () => {
    it("generates float", () => {
        const math = new Mock();
        const rg = new Random(math);

        math.expect_call("random", [0, 1], 0.1337);

        const result = rg.generate_float(0, 1);

        expect(result).toBe(0.1337);
        math.verify();
    });

    it("generates int", () => {
        const math = new Mock();
        const rg = new Random(math);

        math.expect_call("random", [0, 100], 13.37);

        const result = rg.generate_int(0, 100);

        expect(result).toBe(13);
        math.verify();
    });
});
