const Vector2D = require("../lib/vector2d");
const AIGameManager = require("../lib/ai_game_manager");
const Mock = require("./mock");

describe("AIGameManager", () => {
    const initial_pos = new Vector2D(0, 0);

    it("rewards 100 points for food", () => {
        const snake = new Mock();
        const food = new Mock();
        const render = new Mock();
        const board = new Mock();
        const player = new Mock();
        const gm = new AIGameManager(snake, food, board, render, player, 10);

        snake.expect_call("position", [], initial_pos);
        snake.expect_call("grow", []);
        snake.expect_call("update", []);
        snake.expect_call("put", [board]);
        snake.expect_call("is_dead", [], false);

        food.expect_call("position", [], initial_pos);
        food.expect_call("change_position", []);
        food.expect_call("update", []);
        food.expect_call("put", [board]);

        render.expect_call("update", []);

        board.expect_call("update", []);
        board.expect_call("out_of_bounds", [initial_pos], false);

        player.expect_call("update", []);
        player.expect_call("sees_food", [], false);

        gm.update();
        expect(gm.score()).toBe(100);

        snake.verify();
        food.verify();
        render.verify();
        player.verify();
        board.verify();
    });

    it("punishes for dying", () => {
        const pos = new Vector2D(-1, 0);
        const snake = new Mock();
        const food = new Mock();
        const render = new Mock();
        const board = new Mock();
        const player = new Mock();
        const gm = new AIGameManager(snake, food, board, render, player, 10);

        snake.expect_call("position", [], pos);
        snake.expect_call("update", []);
        snake.expect_call("put", [board]);

        food.expect_call("position", [], initial_pos);
        food.expect_call("update", []);
        food.expect_call("put", [board]);

        render.expect_call("update", []);

        board.expect_call("update", []);
        board.expect_call("out_of_bounds", [pos], true);

        player.expect_call("update", []);
        player.expect_call("sees_food", [], false);

        gm.update();
        expect(gm.score()).toBe(-51);

        snake.verify();
        food.verify();
        render.verify();
        player.verify();
        board.verify();
    });

    it("checks number of steps taken", () => {
        const pos = new Vector2D(11, 0);
        const snake = new Mock();
        const food = new Mock();
        const render = new Mock();
        const board = new Mock();
        const player = new Mock();
        const gm = new AIGameManager(snake, food, board, render, player, 0);

        snake.expect_call("position", [], pos);
        snake.expect_call("update", []);
        snake.expect_call("put", [board]);
        snake.expect_call("is_dead", [], false);

        food.expect_call("position", [], initial_pos);
        food.expect_call("update", []);
        food.expect_call("put", [board]);

        render.expect_call("update", []);

        board.expect_call("update", []);
        board.expect_call("out_of_bounds", [pos], false);

        player.expect_call("update", []);
        player.expect_call("sees_food", [], false);

        gm.update();
        expect(gm.is_game_over()).toBe(true);

        snake.verify();
        food.verify();
        render.verify();
        player.verify();
        board.verify();
    });

    it("reward for looking at food", () => {
        const snake = new Mock();
        const food = new Mock();
        const render = new Mock();
        const board = new Mock();
        const player = new Mock();
        const gm = new AIGameManager(snake, food, board, render, player, 0);

        snake.expect_call("position", [], initial_pos);
        snake.expect_call("update", []);
        snake.expect_call("put", [board]);
        snake.expect_call("is_dead", [], false);

        food.expect_call("position", [], initial_pos.add(new Vector2D(0, 1)));
        food.expect_call("update", []);
        food.expect_call("put", [board]);

        render.expect_call("update", []);

        board.expect_call("update", []);
        board.expect_call("out_of_bounds", [initial_pos], false);

        player.expect_call("update", []);
        player.expect_call("sees_food", [], true);

        gm.update();
        expect(gm.score()).toBe(0);

        snake.verify();
        food.verify();
        render.verify();
        player.verify();
        board.verify();
    });
});
