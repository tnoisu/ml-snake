const mathjs = require('mathjs');

class Random {
    constructor(math = mathjs) {
        this.math = math;
    }

    generate_int(min, max) {
        return Math.round(this.generate_float(min, max));
    }

    generate_float(min, max) {
        return this.math.random(min, max);
    }
}

module.exports = Random
