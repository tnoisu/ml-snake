const Vector2D = require("./vector2d");
const HumanPlayer = require("./human_player");
const Snake = require("./snake");
const Food = require("./food");
const GameBoard = require("./game_board");

class GameManagerState {
    is_game_over() {
        return false;
    }

    update(gm) {
        return this;
    }

    render(gm) {
        gm.game_board.render(gm.renderer);
        gm.player.render(gm.renderer);
    }
}

class GameOverState extends GameManagerState {
    constructor(gm) {
        super();
        gm.update_final_score();
    }

    is_game_over() {
        return true;
    }
}

class GameState extends GameManagerState {
    update(gm) {
        const snake_position = gm.snake.position();
        if(snake_position.equals(gm.food.position())) {
            gm.scorepoints = gm.scorepoints + 1;
            gm.food.change_position();
            gm.snake.grow();
        }

        gm.game_board.update();
        gm.snake.update();
        gm.food.update();
        gm.food.put(gm.game_board);
        gm.snake.put(gm.game_board);
        gm.player.update();
        gm.renderer.update();

        if (gm.game_board.out_of_bounds(snake_position)
            || gm.snake.is_dead())
        {
            return new GameOverState(gm);
        }

        return this;
    }
}

class GameManager {
    constructor(snake, food, game_board, renderer, player) {
        this.snake = snake;
        this.food = food;
        this.renderer = renderer;
        this.game_board = game_board;
        this.tick = this.tick.bind(this);
        this.state = new GameState();
        this.player = player;
        this.scorepoints = 0;

        this.final_score_ = new Promise((resolve, reject) => {
            this.update_final_score = () => {
                resolve(this.scorepoints);
            }
        });
    }

    /* istanbul ignore next */
    static create(field_size, renderer) {
        const snake = Snake.create();
        const food = Food.create(field_size);
        const game_board = new GameBoard(field_size);

        const player = new HumanPlayer(window, snake, game_board);

        return new GameManager(snake, food, game_board, renderer, player);
    }

    final_score() {
        return this.final_score_;
    }

    score() {
        return this.scorepoints;
    }

    is_game_over() {
        return this.state.is_game_over();
    }

    game_over() {
        this.state = new GameOverState(this);
    }

    update() {
        this.state = this.state.update(this);
    }

    render() {
        this.state.render(this);
    }

    /* istanbul ignore next */
    tick() {
        this.update();
        this.render();
    }
}

module.exports = {
    GameManager,
    GameManagerState,
    GameState,
    GameOverState,
}
