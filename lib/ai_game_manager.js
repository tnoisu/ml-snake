const {
    GameManager,
    GameState,
    GameOverState
} = require("./game_manager");

class AIGameState extends GameState {
    constructor(max_steps) {
        super();
        this.max_steps = max_steps;
        this.steps = 0;
    }

    punish_for_steps(gm) {
        gm.scorepoints = gm.scorepoints - this.steps;
    }

    update(gm) {
        this.steps++;

        const snake_position = gm.snake.position();
        if(snake_position.equals(gm.food.position())) {
            this.steps = 0;

            gm.scorepoints = gm.scorepoints + 100;
            gm.food.change_position();
            gm.snake.grow();
        }

        gm.game_board.update();
        gm.snake.update();
        gm.food.update();
        gm.food.put(gm.game_board);
        gm.snake.put(gm.game_board);
        gm.player.update();
        gm.renderer.update();

        if (gm.player.sees_food()) {
            gm.scorepoints = gm.scorepoints + 1;
        }

        if (gm.game_board.out_of_bounds(snake_position)
            || gm.snake.is_dead())
        {
            gm.scorepoints = gm.scorepoints - 50;
            this.punish_for_steps(gm);
            return new GameOverState(gm);
        }

        if (this.steps > this.max_steps) {
            this.punish_for_steps(gm);
            return new GameOverState(gm);
        }

        return this;
    }
}

class AIGameManager extends GameManager {
    constructor(snake, food, game_board, renderer, player, max_steps) {
        super(snake, food, game_board, renderer, player, max_steps);
        this.state = new AIGameState(max_steps);
    }
}

module.exports = AIGameManager;
